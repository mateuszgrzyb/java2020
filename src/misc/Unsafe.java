package misc;

import misc.UnsafeRunnable;

public class Unsafe {
    public static void call(UnsafeRunnable r) {
        try {
            r.run();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
