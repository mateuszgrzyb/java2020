package misc;

public interface UnsafeRunnable {
    void run() throws Exception;
}
