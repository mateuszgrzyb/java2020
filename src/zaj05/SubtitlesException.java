package zaj05;

public class SubtitlesException extends java.lang.Exception {
    SubtitlesException(String line, String msg) {
        super(
            String.format(
                "construction error: %s is not a valid subtitle\n%s\n",
                line,
                msg
            )
        );
    }
}
