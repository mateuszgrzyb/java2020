package zaj05;

import java.util.Arrays;

public class Subtitles {

    Subtitles(String line) throws SubtitlesException {

        var split =
            line.split(
                "(?=\\{)|(?<=})"
            );

        if (split.length < 3) {
            throw new SubtitlesException(
                line,
                "possibly bad frame information"
            );
        }

        var first = split[0].substring(1, split[0].length() - 1);

        try {
            framefrom = Integer.parseInt(first);
        } catch (NumberFormatException e) {
            throw new SubtitlesException(
                line,
                String.format("%s is not a number", first)
            );
        }

        var second = split[1].substring(1, split[1].length() - 1);

        try {
            frameto = Integer.parseInt(second);
        } catch (NumberFormatException e) {
            throw new SubtitlesException(
                line,
                String.format("%s is not a number", second)
            );
        }

        if (!(0 <= framefrom && framefrom < frameto)) {
            throw new SubtitlesException(
                line,
                String.format(
                    "from %d to %d is an invalid range of frames",
                    framefrom,
                    frameto
                )
            );
        }

        text = String.join("", Arrays.copyOfRange(split, 2, split.length));
    }

    public void move(int frames) {
        framefrom += frames;
        frameto += frames;
    }

    public boolean checkValidity(int frames) {
        return framefrom > -frames;
    }

    private int framefrom;
    private int frameto;
    private final String text;

    @Override
    public String toString() {
        return String.format(
            "{%d}{%d}%s",
            framefrom,
            frameto,
            text
        );
    }
}

