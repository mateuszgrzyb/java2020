package zaj05;

import zaj03.Int;

import java.io.*;

public class Delay {

    public static void delay(
        String from,
        String to,
        long millis,
        int framerate
    ) throws
        IOException,
        SubtitlesException,
        DelayException
    {

        int frames = (int) (framerate * 1000 / millis);

        try(var r = new BufferedReader(new FileReader(from))) {
            try (var w = new BufferedWriter(new FileWriter(to))) {

                //var line = r.readLine();
                if (new Subtitles(r.readLine()).checkValidity(frames)) {
                    throw new DelayException();
                }
                for (var line : r.lines().toArray(String[]::new)) {
                    var sub = new Subtitles(line);
                    sub.move(frames);
                    w.write(sub.toString());
                    w.newLine();
                }
            }
        }
    }


    public static void main(String[] args) {

        if (args.length != 4) {
            System.out.println("wrong no of args");
            return;
        }

        try {
            Delay.delay(
                args[0],
                args[1],
                Integer.parseInt(args[2]),
                Integer.parseInt(args[3])
            );
        } catch (DelayException | SubtitlesException e) {
            System.out.println(
                e.getMessage()
            );
        } catch (IOException e) {
            System.out.println("IO ERROR");
        } catch (Exception e) {
            System.out.println("Unknown error");
        }
    }
}


