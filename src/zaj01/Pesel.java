package zaj01;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Pesel implements Comparable<Pesel> {

    public Pesel(String repr) throws NumberFormatException {
        if (repr.length() != Size) {
            throw new NumberFormatException("Bad PESEL length");
        }
        nums = repr
                .chars()
                .mapToObj(Character::getNumericValue)
                .toArray(Integer[]::new);
    }

    @Override
    public int compareTo(Pesel pesel) {
        return Arrays.compare(nums, pesel.nums);
    }

    @Override
    public String toString() {
        //return Arrays.toString(nums);
        return Arrays.stream(nums)
                .map(String::valueOf)
                .collect(Collectors.joining(""));
    }

    public static boolean check(Pesel pesel) {
        int controlsum = IntStream
                .range(0, Size-1)
                .map(dec -> control[dec] * pesel.nums[dec])
                .sum();

        return controlsum % 10 == pesel.nums[pesel.nums.length-1];
    }

    private static final int Size = 11;
    private final Integer[] nums;
    private static final int[] control =
            new int[] { 9, 7, 3, 1, 9, 7, 3, 1, 9, 7 };
            //  new int[] { 7, 9, 1, 3, 7, 9, 1, 3, 7, 9 };
}
