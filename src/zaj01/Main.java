package zaj01;

import java.util.Arrays;
import static java.lang.System.out;

public class Main {

    public static void main(String[] args) {
        testPesel();
    }

    public static void test() {
        out.println("Hello World");
        String n = "12345567";
        Integer[] nums = n.chars().mapToObj(Character::getNumericValue).toArray(Integer[]::new);
        out.println(n);
        out.println(Arrays.toString(nums));
    }

    public static void compare(Pesel a, Pesel b) {
        char cmp =  switch (a.compareTo(b)) {
            case -1 -> '<';
            case 0 -> '=';
            case 1 -> '>';
            default -> '?';
        };

        out.printf("%s %c %s\n", a, cmp, b);
    }

    public static void testPesel() {
        var badpesel = new Pesel("44051401358");
        var goodpesel = new Pesel("69042078510");
        var anotherpesel = new Pesel("69042078510");
        out.printf("Pesel %s validity - %b\n", badpesel.toString(), Pesel.check(badpesel));
        out.printf("Pesel %s validity - %b\n", goodpesel.toString(), Pesel.check(goodpesel));
        compare(goodpesel, anotherpesel);
        compare(goodpesel, badpesel);
        compare(goodpesel, new Pesel("98120512345"));
    }

    public static void testDataFrame() {

    }
}
