package zaj01;

import zaj02.Helpers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class DataFrame implements DataFrameInterface {

    public DataFrame(String[] names, String[] types) {
        this(new ArrayList<>(names.length));
        IntStream.range(0, names.length)
            .parallel()
            .forEachOrdered((i) ->
                columns.add(new Series(names[i], types[i]))
            );
    }

    public int size() {
        if (columns.size() == 0) return 0;
        else return columns.get(0).size();
    }

    public SeriesInterface get(String name) {
        return columns.stream()
            .filter(col -> col.getName().equals(name))
            .findFirst()
            .orElse(null);
    }

    public DataFrame get(String[] names, boolean copy) {
        DataFrame df = copy ? copy() : this;
        Set<String> namelist = Set.copyOf(Arrays.asList(names));
        return new DataFrame(df.columns.stream()
            .filter(series -> namelist.contains(series.getName()))
            .collect(Collectors.toCollection(ArrayList::new)));
    }

    public DataFrame addColumn(String name, String type, boolean inplace) {
        DataFrame df = inplace ? this : copy();
        df.columns.add(new Series(name, type));
        return df;
    }

    public DataFrame addRow(String[] data, boolean inplace) {
        DataFrame df = inplace ? this : copy();
        if (data.length == df.columns.size()) {
            IntStream.range(0, data.length)
                .parallel()
                .forEachOrdered(i ->
                    df.columns.get(i).append(data[i])
                );
        }
        return df;
    }

    public void ilocSet(int row, String col, String value) {
        get(col).set(row, value);
    }

    public DataFrame iloc(int row) {
        return new DataFrame(
            Helpers.stream(columns, (s) ->
                s.map((c) ->
                    c.subSeries(row)
                )
            )
        );
    }

    public DataFrame iloc(int from, int to) {
        return new DataFrame(
            Helpers.stream(columns, (s) ->
                s.map((c) ->
                    c.subSeries(from, to)
                )
            )
        );
    }

    // private helpers

    private final ArrayList<Series> columns;

    private DataFrame(ArrayList<Series> columns) {
        this.columns = columns;
    }

    public DataFrame copy() {
        return new DataFrame(
            Helpers.stream(columns, (s) -> s.map(Series::copy))
        );
    }
}

