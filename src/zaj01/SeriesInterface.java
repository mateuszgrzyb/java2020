package zaj01;

public interface SeriesInterface extends Copyable<SeriesInterface> {

    SeriesInterface subSeries(int row);

    SeriesInterface subSeries(int from, int to);

    int size();

    void append(String value);

    void set(int i, String value);

    String getName();
    String getType();
}
