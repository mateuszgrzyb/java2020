package zaj01;

public interface Copyable<T> {
    T copy();
}
