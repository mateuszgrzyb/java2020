package zaj01;

public interface DataFrameInterface extends Copyable<DataFrameInterface> {
    int size();
    SeriesInterface get(String name);
    DataFrameInterface get(String[] names, boolean copy);
    DataFrameInterface addColumn(String name, String type, boolean inplace);
    DataFrameInterface addRow(String[] data, boolean inplace);
    void ilocSet(int row, String col, String value);
    DataFrameInterface iloc(int row);
    DataFrameInterface iloc(int from, int to);
}