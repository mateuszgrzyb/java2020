package zaj01;

import java.util.ArrayList;
import java.util.Collections;

public class Series implements SeriesInterface {

    public Series(String name, String type) {
        this(name, type, new ArrayList<>());
    }

    @Override
    public Series copy() {
        return new Series(name, type, new ArrayList<>(values));
    }

    @Override
    public Series subSeries(int row) {
        return new Series(name, type, new ArrayList<>(Collections.singleton(values.get(row))));
    }

    @Override
    public Series subSeries(int from, int to) {
        return new Series(name, type, new ArrayList<>(values.subList(from, to)));
    }

    @Override
    public String toString() {
        return String.format(
                "Series{type='%s', type='%s'",
                type,
                values.toString()
        );
    }

    @Override
    public int size() {
        return values.size();
    }

    @Override
    public void append(String value) {
        values.add(value);
    }

    @Override
    public void set(int i, String value) {
        values.set(i, value);
    }

    // properties

    private final String name;

    @Override
    public String getName() { return name; }

    private final String type;

    @Override
    public String getType() { return type; }

    // private

    private final ArrayList<String> values;

    private Series(String name, String type, ArrayList<String> values) {
        this.name = name;
        this.type = type;
        this.values = values;
    }
}
