package zaj03;

public class Dbl extends Value {

    @Override
    public String toString() {
        return Double.toString(e);
    }

    @Override
    public Value add(Value v) {
        if (v instanceof Int) {
            return new Int((int) (e + ((Int) v).e));
        } else if (v instanceof Flt) {
            return new Flt((float) (e + ((Flt) v).e));
        } else if (v instanceof Dbl) {
            return new Dbl(e + ((Dbl) v).e);
        } else {
            return this;
        }
    }

    @Override
    public Value sub(Value v) {
        if (v instanceof Int) {
            return new Int((int) (e - ((Int) v).e));
        } else if (v instanceof Flt) {
            return new Flt((float) (e - ((Flt) v).e));
        } else if (v instanceof Dbl) {
            return new Dbl(e - ((Dbl) v).e);
        } else {
            return this;
        }
    }

    @Override
    public Value mul(Value v) {
        if (v instanceof Int) {
            return new Int((int) (e * ((Int) v).e));
        } else if (v instanceof Flt) {
            return new Flt((float) (e * ((Flt) v).e));
        } else if (v instanceof Dbl) {
            return new Dbl(e * ((Dbl) v).e);
        } else {
            return this;
        }
    }

    @Override
    public Value div(Value v) {
        if (v instanceof Int) {
            return new Int((int) (e / ((Int) v).e));
        } else if (v instanceof Flt) {
            return new Flt((float) (e / ((Flt) v).e));
        } else if (v instanceof Dbl) {
            return new Dbl(e / ((Dbl) v).e);
        } else {
            return this;
        }
    }

    @Override
    public Value pow(Value v) {
        if (v instanceof Int) {
            return new Int((int) Math.pow(e, ((Int) v).e));
        } else if (v instanceof Flt) {
            return new Flt((int) Math.pow(e, ((Flt) v).e));
        } else if (v instanceof Dbl) {
            return new Dbl(Math.pow(e, ((Dbl) v).e));
        } else {
            return this;
        }
    }

    @Override
    public boolean eq(Value v) {
        if (v instanceof Int) {
            return e == ((Int) v).e;
        } else if (v instanceof Flt) {
            return e == ((Flt) v).e;
        } else if (v instanceof Dbl) {
            return e == ((Dbl) v).e;
        } else {
            return false;
        }
    }

    @Override
    public boolean lte(Value v) {
        if (v instanceof Int) {
            return e <= ((Int) v).e;
        } else if (v instanceof Flt) {
            return e <= ((Flt) v).e;
        } else if (v instanceof Dbl) {
            return e <= ((Dbl) v).e;
        } else {
            return false;
        }
    }

    @Override
    public boolean gte(Value v) {
        if (v instanceof Int) {
            return e >= ((Int) v).e;
        } else if (v instanceof Flt) {
            return e >= ((Flt) v).e;
        } else if (v instanceof Dbl) {
            return e >= ((Dbl) v).e;
        } else {
            return false;
        }
    }

    @Override
    public boolean neq(Value v) {
        if (v instanceof Int) {
            return e != ((Int) v).e;
        } else if (v instanceof Flt) {
            return e != ((Flt) v).e;
        } else if (v instanceof Dbl) {
            return e != ((Dbl) v).e;
        } else {
            return false;
        }
    }


    @Override
    public boolean equals(Object other) {
        Boolean result = maybeEquals(other);
        if (result != null) return result;
        if (getClass() != other.getClass()) return false;
        Dbl otherI = (Dbl) other;
        return e == otherI.e;
    }

    @Override
    public int hashCode() {
        return Double.hashCode(e);
    }

    @Override
    public Value create(String s) {
        return new Dbl(Integer.parseInt(s));
    }

    // private

    final double e;

    Dbl(double e) {
        this.e = e;
    }
}
