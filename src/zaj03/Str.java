package zaj03;

public class Str extends Value {

    @Override
    public String toString() {
        return e;
    }

    @Override
    public Value add(Value v) {
        if (v instanceof Str) {
            return new Str(e + ((Str) v).e);
        } else {
            return this;
        }
    }

    @Override
    public Value sub(Value v) {
        return this;
    }

    @Override
    public Value mul(Value v) {
        if (v instanceof Int) {
            return new Str(String.valueOf(e).repeat(Math.max(0, ((Int) v).e)));
        } else {
            return this;
        }
    }

    @Override
    public Value div(Value v) {
        return this;
    }

    @Override
    public Value pow(Value v) {
        return this;
    }

    @Override
    public boolean eq(Value v) {
        if (v instanceof Str) {
            return e.compareTo(((Str) v).e) == 0;
        } else {
            return false;
        }
    }

    @Override
    public boolean lte(Value v) {
        if (v instanceof Str) {
            return e.compareTo(((Str) v).e) <= 0;
        } else {
            return false;
        }
    }

    @Override
    public boolean gte(Value v) {
        if (v instanceof Str) {
            return e.compareTo(((Str) v).e) >= 0;
        } else {
            return false;
        }
    }

    @Override
    public boolean neq(Value v) {
        if (v instanceof Str) {
            return e.compareTo(((Str) v).e) != 0;
        } else {
            return false;
        }
    }

    @Override
    public boolean equals(Object other) {
        Boolean result = maybeEquals(other);
        if (result != null) return result;
        if (getClass() != other.getClass()) return false;
        return e.equals(((Str) other).e);
    }

    @Override
    public int hashCode() {
        return e.hashCode();
    }

    @Override
    public Value create(String s) {
        return new Str(s);
    }

    // private

    final String e;

    Str(String e) {
        this.e = e;
    }
}
