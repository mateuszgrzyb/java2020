package zaj03;


import java.time.LocalDateTime;

public class DateTime extends Value {

    @Override
    public String toString() {
        return e.toString();
    }

    @Override
    public Value add(Value v) {
        if (v instanceof DateTime) {
            var ve = ((DateTime) v).e;
            var result = e
                .plusYears(ve.getYear())
                .plusMonths(ve.getMonthValue())
                .plusDays(ve.getDayOfMonth())
                .plusHours(ve.getHour())
                .plusMinutes(ve.getMinute())
                .plusSeconds(ve.getSecond());
            return new DateTime(result);
        } else {
            return this;
        }
    }

    @Override
    public Value sub(Value v) {
        if (v instanceof DateTime) {
            var ve = ((DateTime) v).e;
            var result = e
                .minusYears(ve.getYear())
                .minusMonths(ve.getMonthValue())
                .minusDays(ve.getDayOfMonth())
                .minusHours(ve.getHour())
                .minusMinutes(ve.getMinute())
                .minusSeconds(ve.getSecond());
            return new DateTime(result);
        } else {
            return this;
        }
    }

    @Override
    public Value mul(Value v) {
        return this;
    }

    @Override
    public Value div(Value v) {
        return this;
    }

    @Override
    public Value pow(Value v) {
        return this;
    }

    @Override
    public boolean eq(Value v) {
        if (v instanceof DateTime) {
            return e.compareTo(((DateTime) v).e) == 0;
        } else {
            return false;
        }
    }

    @Override
    public boolean lte(Value v) {
        if (v instanceof DateTime) {
            return e.compareTo(((DateTime) v).e) <= 0;
        } else {
            return false;
        }
    }

    @Override
    public boolean gte(Value v) {
        if (v instanceof DateTime) {
            return e.compareTo(((DateTime) v).e) >= 0;
        } else {
            return false;
        }
    }

    @Override
    public boolean neq(Value v) {
        if (v instanceof DateTime) {
            return e.compareTo(((DateTime) v).e) != 0;
        } else {
            return false;
        }
    }

    @Override
    public boolean equals(Object other) {
        Boolean result = maybeEquals(other);
        if (result != null) return result;
        if (getClass() != other.getClass()) return false;
        return e.equals(((DateTime) other).e);
    }

    @Override
    public int hashCode() {
        return e.hashCode();
    }

    @Override
    public Value create(String s) {
        return new DateTime(LocalDateTime.parse(s));
    }

    final LocalDateTime e;

    DateTime(LocalDateTime e) {
        this.e = e;
    }
}
