package zaj03;

public class Flt extends Value {
    @Override
    public String toString() {
        return Float.toString(e);
    }

    @Override
    public Value add(Value v) {
        if (v instanceof Int) {
            return new Int((int) (e + ((Int) v).e));
        } else if (v instanceof Flt) {
            return new Flt(e + ((Flt) v).e);
        } else if (v instanceof Dbl) {
            return new Dbl(e + ((Dbl) v).e);
        } else {
            return this;
        }
    }

    @Override
    public Value sub(Value v) {
        if (v instanceof Int) {
            return new Int((int) (e - ((Int) v).e));
        } else if (v instanceof Flt) {
            return new Flt(e - ((Flt) v).e);
        } else if (v instanceof Dbl) {
            return new Dbl(e - ((Dbl) v).e);
        } else {
            return this;
        }
    }

    @Override
    public Value mul(Value v) {
        if (v instanceof Int) {
            return new Int((int) (e * ((Int) v).e));
        } else if (v instanceof Flt) {
            return new Flt(e * ((Flt) v).e);
        } else if (v instanceof Dbl) {
            return new Dbl(e * ((Dbl) v).e);
        } else {
            return this;
        }
    }

    @Override
    public Value div(Value v) {
        if (v instanceof Int) {
            return new Int((int) (e / ((Int) v).e));
        } else if (v instanceof Flt) {
            return new Flt(e / ((Flt) v).e);
        } else if (v instanceof Dbl) {
            return new Dbl((double) e / ((Dbl) v).e);
        } else {
            return this;
        }
    }

    @Override
    public Value pow(Value v) {
        if (v instanceof Int) {
            return new Int((int) Math.pow(e, ((Int) v).e));
        } else if (v instanceof Flt) {
            return new Flt((float) Math.pow(e, ((Flt) v).e));
        } else if (v instanceof Dbl) {
            return new Dbl(Math.pow(e, ((Dbl) v).e));
        } else {
            return this;
        }
    }

    @Override
    public boolean eq(Value v) {
        if (v instanceof Int) {
            return e == ((Int) v).e;
        } else if (v instanceof Flt) {
            return e == ((Flt) v).e;
        } else if (v instanceof Dbl) {
            return e == ((Dbl) v).e;
        } else {
            return false;
        }
    }

    @Override
    public boolean lte(Value v) {
        if (v instanceof Int) {
            return e <= ((Int) v).e;
        } else if (v instanceof Flt) {
            return e <= ((Flt) v).e;
        } else if (v instanceof Dbl) {
            return e <= ((Dbl) v).e;
        } else {
            return false;
        }
    }

    @Override
    public boolean gte(Value v) {
        if (v instanceof Int) {
            return e >= ((Int) v).e;
        } else if (v instanceof Flt) {
            return e >= ((Flt) v).e;
        } else if (v instanceof Dbl) {
            return e >= ((Dbl) v).e;
        } else {
            return false;
        }
    }

    @Override
    public boolean neq(Value v) {
        if (v instanceof Int) {
            return e != ((Int) v).e;
        } else if (v instanceof Flt) {
            return e != ((Flt) v).e;
        } else if (v instanceof Dbl) {
            return e != ((Dbl) v).e;
        } else {
            return false;
        }
    }


    @Override
    public boolean equals(Object other) {
        Boolean result = maybeEquals(other);
        if (result != null) return result;
        if (getClass() != other.getClass()) return false;
        Flt otherI = (Flt) other;
        return e == otherI.e;
    }

    @Override
    public int hashCode() {
        return Float.hashCode(e);
    }

    @Override
    public Value create(String s) {
        return new Flt(Integer.parseInt(s));
    }

    // private

    final float e;

    Flt(float e) {
        this.e = e;
    }
}
