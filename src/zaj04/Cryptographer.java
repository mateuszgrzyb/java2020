package zaj04;

import java.io.*;
import java.util.Arrays;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.Scanner;

public class Cryptographer {

    static void cryptfile(
        File from,
        File to,
        Algorithm alg
    ) {
        modifyFile(from, to, alg::crypt);
    }

    static void decryptfile(
        File from,
        File to,
        Algorithm alg
    ) {
        modifyFile(from, to, alg::decrypt);
    }

    private static void modifyFile(
        File from,
        File to,
        Function<String, String> modifier
    ) {

        try {
            var reader = new BufferedReader(new FileReader(from));
            var writer = new BufferedWriter(new FileWriter(to));
            writer.write(
                reader.lines()
                    .map(line ->
                        Arrays.stream(line.split("[\\s]"))
                            //.peek(System.out::println)
                            .map(modifier)
                            //.peek(System.out::println)
                            .collect(Collectors.joining(" "))
                    )
                    .collect(Collectors.joining("\n"))
            );
            reader.close();
            writer.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {

        if (args.length != 2) {
            System.out.println("Wrong no of args.");
            return;
        }

        var in = new Scanner(System.in);
        var from = new File(args[0]);
        var to = new File(args[1]);
        Boolean cipher = null;
        Algorithm alg = null;

        while (cipher == null) {
            System.out.println("Do you want to cipher or decipher? [d/c]");
            switch (in.next()) {
                case "d", "decipher" -> cipher = true;
                case "c", "cipher" -> cipher = false;
                default -> System.out.println("What?");
            }
        }

        while (alg == null) {
            System.out.println("What algorithm? ROT11 or Polybius? [r/p]");
            switch (in.next()) {
                case "r", "rot11" -> alg = new ROT11();
                case "p", "polybius" -> alg = new Polibiusz();
                default -> System.out.println("What?");
            }
        }

        if (cipher) Cryptographer.cryptfile(from, to, alg);
        else Cryptographer.decryptfile(from, to, alg);

        System.out.println("Success!");
    }
}

