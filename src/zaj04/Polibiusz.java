package zaj04;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class Polibiusz implements Algorithm {

    @Override
    public String crypt(String word) {
        return word.chars()
            .mapToObj(c -> Integer.toString(Polibiusz.get((char) c)))
            .collect(Collectors.joining());
    }

    @Override
    public String decrypt(String word) {
        var counter = new AtomicInteger();
        return word
            .chars()
            .mapToObj(c -> Character.getNumericValue((char) c))
            .collect(Collectors.groupingBy(it -> counter.getAndIncrement() / 2))
            .values()
            .stream()
            .map(list -> Polibiusz.get(list.get(0), list.get(1)))
            .collect(
                StringBuilder::new,
                StringBuilder::appendCodePoint,
                StringBuilder::append
            ).toString();
    }

    // szachownica
    //
    //     1  2  3  4  5  6
    //  1  !  "  ,  .  ?  a
    //  2  b  c  d  e  f  g
    //  3  h  i  j  k  l  m
    //  4  n  o  p  q  r  s
    //  5  t  u  w  x  y  z


    private static final ArrayList<Character> alphabet =
        "!\",.?abcdefghijklmnopqrstuwxyz"
            .chars()
            .mapToObj(e -> (char) e)
            .collect(Collectors.toCollection(ArrayList::new));



    private static char get(int row, int col) {
        if (
            1 <= row && row <= 5 &&
            1 <= col && col <= 6
        ) return alphabet.get(6 * (row-1) + (col-1));
        else return (char) -1;
    }

    private static int get(char c) {
        int pos = Collections.binarySearch(alphabet, c);
        int row = pos / 6;
        int col = pos - row * 6;
        return (row + 1) * 10 + col + 1;
    }

    public static void main(String[] args) {
        var p = new Polibiusz();
        var c = p.crypt("zygfryd");
        var d = p.decrypt(c);
        System.out.println(d);
    }
}

