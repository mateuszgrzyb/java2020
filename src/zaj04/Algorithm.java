package zaj04;

import java.util.function.Function;

public interface Algorithm {
    String crypt(String word);
    String decrypt(String word);
}
