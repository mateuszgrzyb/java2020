package zaj04;

import java.util.function.Function;

public class ROT11 implements Algorithm {

    @Override public String crypt(String word) {
        return modify(word, ROT11::rotate);
    }

    @Override public String decrypt(String word) {
        return modify(word, ROT11::unrotate);
    }

    private String modify(String word, Function<Character, Character> modifier) { return word
        .chars()
        .mapToObj(c -> (char) c)
        .map(modifier)
        .collect(
            StringBuilder::new,
            StringBuilder::appendCodePoint,
            StringBuilder::append
        ).toString();
    }

    private static char rotate(char c) {
        if (Character.isAlphabetic(c)) {
            var c1 = c + 11;
            if (Character.isUpperCase(c)) {
                if (c1 <= 'Z') return (char) c1;
                else return (char) ('A' + (c1 - 'Z'));
            } else {
                if (c1 <= 'z') return (char) c1;
                else return (char) ('a' + (c1 - 'z'));
            }
        }
        return c;
    }

    private static char unrotate(char c) {
        if (Character.isAlphabetic(c)) {
            var c1 = c - 11;
            if (Character.isUpperCase(c)) {
                if ('A' <= c1) return (char) c1;
                else return (char) ('Z' + ('A' - c1));
            } else {
                if ('a' <= c1) return (char) c1;
                else return (char) ('z' + ('a' - c1));
            }
        }
        return c;
    }


    private final static int rotation = 11;
}
