package zaj12;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.function.Function;

public class Program {
    public static void main(String[] argv) {
        new Program(600, 600);
        //Test.test();
    }

    public Program(int width, int height) {
        window.setSize(width, height);
        window.setVisible(true);

        window.add(new JPanel() {{
            setLayout(new BorderLayout());
            add(canvas, BorderLayout.CENTER);
            add(inputs, BorderLayout.PAGE_END);
        }});


        window.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });
    }

    JFrame window = new JFrame();
    Canvas canvas = new Canvas();
    Inputs inputs = new Inputs(canvas);
}


class Init {
    public static <T> T init(T t, Consumer<T> fun) {
        fun.accept(t);
        return t;
    }
}

class Test {
    public static void test() {
        List<Long> times = new ArrayList<>();
        List<Map<String, String>> maps = new ArrayList<>();

        times.add(System.nanoTime());

        maps.add(Init.init(
            new HashMap<>(), (map) -> {
                map.put("name", "Ala");
                map.put("does", "ma");
                map.put("what", "kota");
            }
        ));

        times.add(System.nanoTime());

        maps.add(
            ((Function<Map<String, String>, Map<String, String>>)
                map -> {
                    map.put("name", "Ala");
                    map.put("does", "ma");
                    map.put("what", "kota");
                    return map;
                }).apply(new HashMap<>()));

        times.add(System.nanoTime());

        maps.add(new HashMap<>() {{
            put("name", "Ala");
            put("does", "ma");
            put("what", "kota");
        }});

        times.add(System.nanoTime());

        Map<String, String> map = new HashMap<>();
        map.put("name", "Ala");
        map.put("does", "ma");
        map.put("what", "kota");
        maps.add(map);

        times.add(System.nanoTime());

        for (int i = 1; i < times.size(); i++) {
            System.out.printf("Panel %s\n time: %d\n\n",
                maps.get(i-1),
                times.get(i) - times.get(i-1));
        }
    }
}