package zaj12;

import zaj12.Shapes.Drawable;


import javax.swing.*;
import java.awt.*;
import java.util.ArrayDeque;

public class Canvas extends JPanel {

    class Erase implements Drawable {

        @Override
        public void draw(Graphics arg0, int x, int y) {
            Canvas.super.paint(arg0);
        }
    }

    @Override
    public void paint(Graphics arg0) {
        if (drawing) {
            System.out.println("Drawing");

            drawable.draw(arg0, x, y);

            this.x = getWidth() / 2;
            this.y = getHeight() / 2;
            drawing = false;
        } else {
            System.out.println("Resizing");
        }
    }

    public void draw(Drawable drawable, int x, int y) {
        drawing = true;
        this.drawable = drawable;
        this.x = x;
        this.y = y;
        revalidate();
        repaint();
    }

    private Drawable drawable;
    private int x;
    private int y;
    private boolean drawing = false;

    public final Drawable erase = new Erase();
}

