package zaj12;

import zaj12.Shapes.*;

import javax.swing.*;
import java.util.ArrayList;

public class Inputs extends JPanel {
    class JEraseButton extends JButton {
        public JEraseButton(String text) {
            super(text);
            addActionListener((l) -> {
                canvas.draw(canvas.erase, 0, 0);
            });
        }
    }

    class JDrawButton extends JButton {
        public JDrawButton(String text) {
            super(text);
            addActionListener((l) -> {
                Integer x = xIntField.getNumber();
                Integer y = yIntField.getNumber();
                Drawable s = (Drawable) options.getSelectedItem();
                if (x != null && y != null) {
                    canvas.draw(s, x, y);
                }
            });
        }
    }

    public Inputs(Canvas canvas) {
        this.canvas = canvas;
        setLayout(new MyLayout(this) {{
            addComponents(
                new JDrawButton("Draw Selected"),
                new JEraseButton("Erase All"),
                xIntField,
                yIntField,
                options
            );
        }});
        /*
        var layout = new MyLayout(this);
        setLayout(layout);
        layout.addComponents(
            new JDrawButton("Draw Selected"),
            new JEraseButton("Erase All"),
            xIntField,
            yIntField,
            options
        );

         */
    }

    private final Canvas canvas;
    private final MyIntField xIntField = new MyIntField();
    private final MyIntField yIntField = new MyIntField();
    private final JComboBox<Drawable> options = new JComboBox<>(
        new Drawable[]{ new Rectangle(), new Circle(), new Text() }
    );
}

