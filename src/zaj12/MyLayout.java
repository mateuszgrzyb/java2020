package zaj12;

import javax.swing.*;
import java.awt.*;

public class MyLayout extends GroupLayout {
    public MyLayout(Container container) {
        super(container);
    }

    public void addComponents(JComponent... components) {

        setAutoCreateGaps(true);
        setAutoCreateContainerGaps(true);
        ParallelGroup verticalGroup = createParallelGroup();
        SequentialGroup horizontalGroup = createSequentialGroup();
        for (var c : components) {
            verticalGroup.addComponent(c);
            horizontalGroup.addComponent(c);
        }
        setVerticalGroup(verticalGroup);
        setHorizontalGroup(horizontalGroup);
    }
}
