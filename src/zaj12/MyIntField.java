package zaj12;

import javax.swing.*;

public class MyIntField extends JTextField {
    public Integer getNumber() {
        try {
            return Integer.parseInt(super.getText());
        } catch (NumberFormatException e) {
            return null;
        }
    }
}
