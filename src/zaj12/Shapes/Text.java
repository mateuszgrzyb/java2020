package zaj12.Shapes;

import java.awt.*;

public class Text implements Drawable {

    @Override
    public String toString() {
        return "Text";
    }

    @Override
    public void draw(Graphics arg0, int x, int y) {
        String s = "Ala ma kota";
        arg0.setColor(Color.red);
        arg0.setFont(new Font("SansSerif", Font.BOLD, 30));
        arg0.drawString(s, x, y);
    }
}
