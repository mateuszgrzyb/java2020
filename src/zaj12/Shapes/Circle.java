package zaj12.Shapes;

import java.awt.*;
import java.awt.geom.Ellipse2D;

public class Circle implements Drawable {

    @Override
    public String toString() {
        return "Circle";
    }

    @Override
    public void draw(Graphics arg0, int x, int y) {
        int r = 200;
        Graphics2D g2 = (Graphics2D) arg0;
        Ellipse2D e = new Ellipse2D.Double(x, y, r, r);

        GradientPaint gp = new GradientPaint(
            x - r / 2f, y - r / 2f, Color.YELLOW,
            x + r / 2f, y + r / 2f, Color.BLUE,
            false
        );

        g2.setPaint(gp);
        g2.fill(e);
        g2.draw(e);
    }
}
