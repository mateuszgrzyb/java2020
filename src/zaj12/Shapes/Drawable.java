package zaj12.Shapes;

import java.awt.*;

public interface Drawable {
    void draw(Graphics arg0, int x, int y);
}
