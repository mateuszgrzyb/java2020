package zaj12.Shapes;

import java.awt.*;

public class Rectangle implements Drawable {

    @Override
    public String toString() {
        return "Rectangle";
    }

    @Override
    public void draw(Graphics arg0, int x, int y) {
        Graphics2D g = (Graphics2D) arg0;
        g.setStroke(new BasicStroke(5));
        g.drawRect(x, y, 150, 100);
    }
}
