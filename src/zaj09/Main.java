package zaj09;

import java.io.IOException;

public class Main {
    public static void main(String[] args) {

        new Thread(() -> {
            try {
                EchoServer.main(args);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }).start();

        new Thread(() -> {
            try {
                EchoClient.main(args);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }).start();
    }
}
