package zaj09;

import zaj03.Int;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.function.BiFunction;


public class EchoServer {
    public static void main(String[] args) throws IOException {
        ServerSocket serverSocket = null;
        try {
            serverSocket = new ServerSocket(6666);
        } catch (IOException e) {
            System.out.println("Could not listen on port: 6666");
            System.exit(-1);
        }

        Socket clientSocket = null;
        try {
            clientSocket = serverSocket.accept();
        } catch (IOException e) {
            System.out.println("Accept failed: 6666");
            System.exit(-1);
        }
        PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
        BufferedReader in = new BufferedReader(
            new InputStreamReader(
                clientSocket.getInputStream()));
        String inputLine;

        while ((inputLine = in.readLine()) != null) {
            out.printf("Server says: %s\n", maybeCalc(inputLine));
        }
        out.close();
        in.close();
        clientSocket.close();
        serverSocket.close();

    }

    private static String maybeCalc(String str) {
        var map = new HashMap<String, BiFunction<Integer, Integer, String>>() {{
            put("+", (a, b) -> String.valueOf(a + b));
            put("-", (a, b) -> String.valueOf(a - b));
            put("*", (a, b) -> String.valueOf(a * b));
            put("/", (a, b) -> String.valueOf(((float) a / b)));
        }};

        for (var key : map.keySet()) {
            if (str.contains(key)) return _maybeCalc(str, key, map.get(key));
        }

        return str;
    }

    private static String _maybeCalc(
        String str,
        String op,
        BiFunction<Integer, Integer, String> fun
        ) {
        var strs = str.split("[" + op + "]");
        if (strs[0].equals(str)) return str;
        if (strs.length != 2) return str;
        var a = maybeInteger(strs[0]);
        var b = maybeInteger(strs[1]);
        if (a == null || b == null) return str;
        return fun.apply(a, b);
    }

    private static Integer maybeInteger(String str) {
        try {
            return Integer.parseInt(str);
        } catch (NumberFormatException e) {
            return null;
        }
    }
}


