package zaj11;

import java.io.*;
import java.net.Socket;
import java.util.List;

public class EchoClient {

    public EchoClient(int port) throws IOException {
        s = new Socket("localhost", port);
        out = new PrintWriter(s.getOutputStream(), true);
        in = new ObjectInputStream(s.getInputStream());
    }

    @SuppressWarnings("unchecked")
    public List<Integer> findPrimes(int min, int max) {
        try {

            out.printf("%d;%d\n", min, max);
            return (List<Integer>) in.readObject();

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void main(String[] args) throws IOException {
        var c = new EchoClient(6666);
        System.out.println(c.findPrimes(2, 1000));
        System.out.println(c.findPrimes(3, 12));

        c.close();
    }

    public void close() {
        try {
            s.close();
            out.close();
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private final Socket s;
    private final PrintWriter out;
    private final ObjectInputStream in;
}
