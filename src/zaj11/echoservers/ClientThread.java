package zaj11.echoservers;

import zaj11.PrimeFinder;

import java.io.*;
import java.net.Socket;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ClientThread extends Thread {

    ClientThread(Socket socket) throws IOException {
        c = socket;
        out = new ObjectOutputStream(c.getOutputStream());
        in = new BufferedReader(
            new InputStreamReader(
                c.getInputStream()
            )
        );
    }

    @Override
    public void run() {

        while (!c.isClosed()) {

            try {
                List<Optional<?>> stream =  in.lines().map((str) -> {
                    if (str.equals("quit")) return Optional.empty();
                    else {
                        var strings = str.split(";");
                        var min = Integer.parseInt(strings[0]);
                        var max = Integer.parseInt(strings[1]);
                        return Optional.of(PrimeFinder.run(min, max));
                    }
                }).collect(Collectors.toList());

                var input = in.readLine();
                System.out.println(input);

                if (input == null) {
                    sleep(1000);
                    continue;
                }

                if (
                    input.equals("quit")
                ) break;

                var strings = input.split(";");
                var min = Integer.parseInt(strings[0]);
                var max = Integer.parseInt(strings[1]);

                out.writeObject(PrimeFinder.run(min, max));
            } catch (IOException | InterruptedException e) {
                e.printStackTrace();
            }
        }

        close();
    }

    public void close() {
        try {
            c.close();
            out.close();
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private final Socket c;
    private final ObjectOutputStream out;
    private final BufferedReader in;
}
