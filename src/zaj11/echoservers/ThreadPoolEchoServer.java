package zaj11.echoservers;

import zaj11.threadpools.ThreadPool;

import java.io.IOException;

public class ThreadPoolEchoServer extends EchoServer {

    public ThreadPoolEchoServer(int port) throws IOException {
        super(port);
        tp = new ThreadPool();
    }

    public void start() {
        new Thread(() -> {
            while (running) {
                try {
                    tp.addTask(new ClientThread(s.accept()));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    private final ThreadPool tp;
}
