package zaj11.echoservers;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.ArrayList;
import java.util.List;

public abstract class EchoServer {

    public EchoServer(int port) throws IOException {
        s = new ServerSocket(port);
    }

    public abstract void start();

    public void close() {
        running = false;
        try {
            s.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    protected final ServerSocket s;
    protected volatile boolean running = true;
}
