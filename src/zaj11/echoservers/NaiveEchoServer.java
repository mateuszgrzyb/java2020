package zaj11.echoservers;

import zaj11.echoservers.ClientThread;
import zaj11.echoservers.EchoServer;

import java.io.IOException;

public class NaiveEchoServer extends EchoServer {

    public NaiveEchoServer(int port) throws IOException {
        super(port);
    }

    public void start() {
        new Thread(() -> {
            while (running) {
                try {
                    new ClientThread(s.accept()).start();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }
}


