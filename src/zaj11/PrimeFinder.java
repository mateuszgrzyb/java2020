package zaj11;

import java.util.*;

import static java.lang.System.out;

public class PrimeFinder {
    public static List<Integer> run(int min, int max) {

        List<Integer> result = new ArrayList<>();
        boolean[] prime = new boolean[max+1];

        for (int p = 2; p*p <= max; p++)
            if (!prime[p]) {
                for (int i = p * p; i <= max; i += p) {
                    prime[i] = true;
                }
            }

        for(int i = Math.max(2, min); i <= max; i++) {
            if(!prime[i]) {
                result.add(i);
            }
        }

        out.println();
        return result;
    }

    public static void main(String[] args) {

        var in = new Scanner(System.in);
        //var min = in.nextInt();
        //var max = in.nextInt();

        out.println(PrimeFinder.run(12, 1000000000));
    }
}
