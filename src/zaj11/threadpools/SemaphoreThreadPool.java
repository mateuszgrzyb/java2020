package zaj11.threadpools;

import java.util.ArrayDeque;
import java.util.Queue;
import java.util.concurrent.Semaphore;

public class SemaphoreThreadPool {

    class Worker extends Thread {

        @Override
        public void run() {
            while (working) {
                Runnable task = taskQueue.poll();
                if (task == null) {
                    semaphore.acquireUninterruptibly();
                    continue;
                }
                task.run();
            }
        }
    }

    public SemaphoreThreadPool(int size) {
        for (int i = 0; i < size; i++) {
            new Worker().start();
        }
    }

    public SemaphoreThreadPool() {
        this(Runtime.getRuntime().availableProcessors() - 1);
    }

    public void addTask(Runnable task) {
        taskQueue.add(task);
        semaphore.release();
    }

    public void stop() {
        working = false;
    }

    private final Queue<Runnable> taskQueue = new ArrayDeque<>();
    private final Semaphore semaphore = new Semaphore(0);
    private volatile boolean working = true;
}
