package zaj11.threadpools;

import misc.Unsafe;

import java.util.ArrayDeque;
import java.util.Queue;

public class ThreadPool {

    class Worker extends Thread {

        @Override
        public void run() {
            while (working) {
                Runnable task = taskQueue.poll();
                synchronized (lock) {
                    if (task == null) {
                        try {
                            lock.wait();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        continue;
                    }
                }
                task.run();
            }
        }
    }

    public ThreadPool(int size) {
        for (int i = 0; i < size; i++) {
            new Worker().start();
        }
    }

    public ThreadPool() {
        this(Runtime.getRuntime().availableProcessors() - 1);
    }

    public void addTask(Runnable task) {
        synchronized (lock) {
            taskQueue.add(task);
            lock.notify();
        }
    }

    public void stop() {
        working = false;
    }

    private final Queue<Runnable> taskQueue = new ArrayDeque<>();
    private final Object lock = new Object();
    private volatile boolean working = true;
}


