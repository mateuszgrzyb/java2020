package zaj11;

import zaj11.echoservers.ThreadPoolEchoServer;

import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {
        var port = 6666;
        var tp = new ThreadPoolEchoServer(port);
        tp.start();
    }
}
