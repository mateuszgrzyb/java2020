package zaj11;

import misc.Unsafe;
import zaj11.threadpools.ThreadPool;

import java.io.IOException;

public class Test {
    public static void test() throws IOException {
        var th = new ThreadPool(2);
        th.addTask(() -> blockingFunction(1, 200L, 2));
        Unsafe.call(() -> Thread.sleep(100));
        th.addTask(() -> blockingFunction(2, 200L, 1));
        Unsafe.call(() -> Thread.sleep(100));
        th.addTask(() -> blockingFunction(3, 200L, 2));
        Unsafe.call(() -> Thread.sleep(4000));
        th.addTask(() -> blockingFunction(4, 200L, 2));

    }

    public static void blockingFunction(int id, long mDelay, int sTime) {
        for (long rem = 0; rem < (sTime * 1000L) + mDelay; rem += mDelay) {
            System.out.printf("%d: working for %d millis\n", id, rem);
            Unsafe.call(() -> Thread.sleep(mDelay));
        }
    }

    public static int test2() {

        Integer a = null;

        a.toString();

        try {
            return 0;
        } catch (Exception e) {
            return 1;
        } finally {
            return 2;
        }
    }

    public static void main(String[] args) {
        System.out.println(test2());
    }
}

