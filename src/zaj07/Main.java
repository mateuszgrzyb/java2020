package zaj07;


import java.util.*;

import static java.lang.System.*;

public class Main {
    public static void main(String[] args) {
        var hive = new Hive();
        var in = new Scanner(System.in);
        int n = 0;
        List<Thread> threads = new ArrayList<>();

        while (true) {
            out.println("How many bees?");
            try {
                n = Integer.parseInt(in.next());
            } catch (NumberFormatException e) {
                out.println("Please enter integer value");
                continue;
            }
            if (n <= 0)
                out.println("Please enter positive value");
            else
                break;
        }

        for (var i = 0; i < n; i++) {
            var thread = new Thread(new Bee(hive, i));
            threads.add(thread);
            thread.start();
        }

    }

    public static Integer strToMaybeInt(String str) {
        try {
            return Integer.parseInt(str);
        } catch (NumberFormatException e) {
            return null;
        }
    }
}
