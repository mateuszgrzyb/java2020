package zaj07;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Hive {

    private List<Door> doors = new ArrayList<Door>() {{
        this.add(new Door());
        this.add(new Door());
    }};

    boolean occupyDoor(int n) {
        return doors.get(n-1).occupy();
    }

    void freeDoor(int n) {
        doors.get(n-1).free();
    }

}


