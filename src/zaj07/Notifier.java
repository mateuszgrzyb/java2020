package zaj07;

import static java.lang.System.out;

public class Notifier {
    public static void notifyEnter(Bee bee, int door) {
        out.printf("Bee %d enters door %d\n", bee.id, door);
    }

    public static void notifyExit(Bee bee, int door) {
        out.printf("Bee %d exits door %d\n", bee.id, door);
    }

    public static void notifyCheck1(Bee bee) {
        out.printf("Bee %d checks door 1\n", bee.id);
    }

    public static void notifyCheck2(Bee bee) {
        out.printf("Bee %d checks door 2 - door 1 occupied\n", bee.id);
    }

    public static void notifyAwait(Bee bee) {
        out.printf("Bee %d waits on door 2 - door 2 occupied\n", bee.id);
    }
}
