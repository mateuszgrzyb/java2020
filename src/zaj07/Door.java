package zaj07;

public class Door {
    private boolean occupied = false;

    boolean occupy() {
        if (occupied) return false;
        else {
            occupied = true;
            return true;
        }
    }

    void free() {
        occupied = false;
    }
}
