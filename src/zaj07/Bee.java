package zaj07;


import javax.imageio.plugins.tiff.TIFFDirectory;
import java.util.Random;

import static java.lang.System.*;

public class Bee implements Runnable {


    public Bee(Hive hive, int id) {
        this.hive = hive;
        this.id = id;
    }


    @Override
    public void run() {
        while (true) {
            goThroughDoor();
            Timer.sleep(Timer.millisIn());
            goThroughDoor();
            Timer.sleep(Timer.millisOut());
        }
    }

    private void goThroughDoor() {

        int door = 0;

        while (door == 0) {

            synchronized (hive) {
                Notifier.notifyCheck1(this);
                if (hive.occupyDoor(1)) {
                    Notifier.notifyEnter(this, 1);
                    door = 1;
                }
            }

            synchronized (hive) {
                Notifier.notifyCheck2(this);
                if (hive.occupyDoor(2)) {
                    Notifier.notifyEnter(this, 2);
                    door = 2;
                }
            }

            Notifier.notifyAwait(this);

            //deadlocks?
            //hive.wait();
            Timer.sleep(200);


        }

        Timer.sleep(Timer.millisThrough);

        synchronized (hive) {
            Notifier.notifyExit(this, door);
            hive.freeDoor(door);
            //hive.notify();
        }

    }

    private final Hive hive;
    public final int id;
}

