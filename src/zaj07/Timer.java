package zaj07;

import java.util.Random;

class Timer {

    static public final long millisThrough = 1000;

    static public long millisIn() {
        return randRange(inMin, inMax);
    }

    static public long millisOut() {
        return randRange(outMin, outMax);
    }

    static private final long inMin = 1000;
    static private final long inMax = 5000;
    static private final long outMin = 5000;
    static private final long outMax = 10000;
    static private final Random r = new Random();

    static private long randRange(long min, long max) {
        return (r.nextInt((int) ((max - min) + 1)) + min);
    }

    static public void sleep(long l) {
        try {
            Thread.sleep(l);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
