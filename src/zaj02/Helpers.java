package zaj02;

import java.util.ArrayList;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Helpers {

    public static <T, U> ArrayList<U> stream(
            ArrayList<T> values,
            Function<Stream<T>, Stream<U>> modifier
    ) {
        return modifier.apply(values.stream()).collect(Collectors.toCollection(ArrayList::new));
    }

    public static <T, U, V> ArrayList<V> zipWith(T[] t, U[] u, BiFunction<T, U, V> fun) {
        ArrayList<V> result = new ArrayList<>();
        IntStream.range(0, Math.min(t.length, u.length))
            .parallel()
            .forEachOrdered((i) ->
                result.add(fun.apply(t[i], u[i]))
            );
        return result;
    }
}
