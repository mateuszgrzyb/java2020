package zaj02;

import zaj01.DataFrameInterface;
import zaj01.Series;
import zaj01.SeriesInterface;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Set;
import java.util.stream.IntStream;

public class GenericDataFrame<S extends SeriesInterface> implements DataFrameInterface {

    public GenericDataFrame(String[] names, String[] types) {
        //this(Helpers.zipWith(names, types, this::newS));
        this(null);
        this.columns = Helpers.zipWith(names, types, this::newS);
    }

    @Override
    public int size() {
        if (columns.size() == 0) return 0;
        else return columns.get(0).size();
    }

    @Override
    public SeriesInterface get(String name) {
        return columns.stream()
            .filter(c -> c.getName().equals(name))
            .findFirst()
            .orElse(null);
    }

    @Override
    public DataFrameInterface get(String[] names, boolean copy) {
        GenericDataFrame<S> df = copy ? (GenericDataFrame<S>) copy() : this;
        Set<String> namelist = Set.copyOf(Arrays.asList(names));
        return new GenericDataFrame<S>(
            Helpers.stream(df.columns, s ->
                s.filter(series -> namelist.contains(series.getName()))
            )
        );
    }

    @Override
    public DataFrameInterface addColumn(String name, String type, boolean inplace) {
        GenericDataFrame<S> df = inplace ? this : (GenericDataFrame<S>) copy();
        //df.columns.add(new S(name, type))
        df.columns.add(newS(name, type));
        return df;
    }

    @Override
    public DataFrameInterface addRow(String[] data, boolean inplace) {
        GenericDataFrame<S> df = inplace ? this : (GenericDataFrame<S>) copy();
        if (data.length == df.columns.size()) {
            IntStream.range(0, data.length)
                .parallel()
                .forEachOrdered(i ->
                    df.columns.get(i).append(data[i])
                );
        }
        return df;
    }

    @Override
    public void ilocSet(int row, String col, String value) {
        get(col).set(row, value);
    }

    @Override
    public DataFrameInterface iloc(int row) {
        return new GenericDataFrame<>(
            Helpers.stream(columns, (s) ->
                s.map((c) ->
                    c.subSeries(row)
                )
            )
        );
    }

    @Override
    public DataFrameInterface iloc(int from, int to) {
        return new GenericDataFrame<>(
            Helpers.stream(columns, (s) ->
                s.map((c) ->
                    c.subSeries(from, to)
                )
            )
        );
    }

    @Override
    public DataFrameInterface copy() {
        return new GenericDataFrame<S>(
            Helpers.stream(columns, (s) -> s.map((e) -> (S) e.copy()))
        );
    }

    // private

    private ArrayList<S> columns;

    private final Class<S> clazz;

    private GenericDataFrame(ArrayList<S> columns) {
        this.columns = columns;
        this.clazz = (Class<S>) this.getClass().getGenericSuperclass();
    }

    private S newS(String name, String type) {
        try {
            return clazz
                .getConstructor(String.class, String.class)
                .newInstance(name, type);
        } catch (Exception e) {
            System.out.println("ERROR: Bad code");
            return null;
        }
    }
}
