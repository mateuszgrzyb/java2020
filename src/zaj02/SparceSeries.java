package zaj02;

import zaj01.Series;
import zaj01.SeriesInterface;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Optional;
import java.util.TreeSet;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class SparceSeries implements SeriesInterface {

    public SparceSeries(String name, String type) {
        this(name, type, new ArrayList<>());
    }

    @Override
    public SparceSeries copy() {
        return new SparceSeries(name, type, new ArrayList<>(values));
    }

    @Override
    public SparceSeries subSeries(int row) {
        return new SparceSeries(
                name,
                type,
                Helpers.stream(values, (s) -> s.filter((coov) -> coov.i == row))
        );
    }

    @Override
    public SparceSeries subSeries(int from, int to) {
        return new SparceSeries(
                name,
                type,
                Helpers.stream(values, (s) -> s.filter((coov) -> from <= coov.i && coov.i < to )));
                /*
                values.stream().filter((coov) ->
                        from <= coov.i && coov.i < to
                ).collect(Collectors.toCollection(ArrayList::new)));
                 */
    }

    @Override
    public int size() {
        return values.size();
    }

    @Override
    public void append(String value) {
        values.add(new COOValue(size(), value));
    }

    @Override
    public void set(int i, String value) {
        values = Helpers.stream(values, (s) -> s.map((coov) -> {
            if (coov.i == i) return new COOValue(i, value);
            else return coov;
        }));
    }


    // private

    private final String name;

    @Override
    public String getName() { return name; }

    private final String type;

    @Override
    public String getType() { return type; }

    private ArrayList<COOValue> values;

    private SparceSeries(String name, String type, ArrayList<COOValue> values) {
        this.name = name;
        this.type = type;
        this.values = values;
    }

}
