package zaj02;
import zaj01.*;

import java.util.ArrayList;


public class SparceDataFrame extends GenericDataFrame<SparceSeries> {
    public SparceDataFrame(String[] names, String[] types) {
        super(names, types);
    }

    public SparceDataFrame(DataFrame df) {
        super(null, null);
    }
}
/*
public class SparceDataFrame implements DataFrameInterface {
    public SparceDataFrame(String[] names, String[] types, String hide) {
        this(Helpers.zipWith(names, types, SparceSeries::new), hide);
    }

    public SparceDataFrame(DataFrame df, String hide) {
    }


    @Override
    public int size() {
        return 0;
    }

    @Override
    public SeriesInterface get(String name) {
        return null;
    }

    @Override
    public SparceDataFrame get(String[] names, boolean copy) {
        return null;
    }

    @Override
    public SparceDataFrame addColumn(String name, String type, boolean inplace) {
        return null;
    }

    @Override
    public SparceDataFrame addRow(String[] data, boolean inplace) {
        return null;
    }

    @Override
    public void ilocSet(int row, String col, String value) {

    }

    @Override
    public SparceDataFrame iloc(int row) {
        return null;
    }

    @Override
    public SparceDataFrame iloc(int from, int to) {
        return null;
    }

    // private

    private ArrayList<SparceSeries> columns;

    private String hide;

    private SparceDataFrame(ArrayList<SparceSeries> columns, String hide) {
        this.columns = columns;
        this.hide = hide;
    }

    public SparceDataFrame copy() {
        return new SparceDataFrame(
            Helpers.stream(columns, (s) -> s.map(SparceSeries::copy)),
            hide
        );
    }

}
*/